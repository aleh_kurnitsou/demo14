package com.epam.gomel.tat2015.demo14.lib.feature.mail.screen;

import com.epam.gomel.tat2015.demo14.framework.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class InboxPage extends MailboxBasePage {
    private By currentAddressElement = By.cssSelector("#nb-1");

    public String getCurrentAddress() {
        return Browser.current().getText(currentAddressElement).trim();

    }
}
