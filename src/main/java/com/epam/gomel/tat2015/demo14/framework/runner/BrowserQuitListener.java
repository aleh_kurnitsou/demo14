package com.epam.gomel.tat2015.demo14.framework.runner;

import com.epam.gomel.tat2015.demo14.framework.ui.Browser;
import org.testng.ITestContext;
import org.testng.TestListenerAdapter;

/**
 * @author Aleh_Vasilyeu
 */
public class BrowserQuitListener extends TestListenerAdapter {

    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);
        if (Browser.current() != null) {
            Browser.current().quit();
        }
    }
}
