package com.epam.gomel.tat2015.demo14.framework.config;

import com.epam.gomel.tat2015.demo14.framework.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite.ParallelMode;

import java.util.List;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class GlobalConfig {

    private static GlobalConfig instance;

    @Option(name = "-bt", aliases = {"--browser_type"}, usage = "browser type: firefox or chrome")
    private BrowserType browserType = BrowserType.FIREFOX;

    @Option(name = "-s", aliases = {"--suites"}, usage = "list of pathes to suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites = null;

    @Option(name = "-pm", usage = "parallel mode: false or tests")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-tc", aliases = {"--thread_count"}, usage = "amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    @Option(name = "-hub", usage = "selenium hub")
    private String seleniumHub = null;

    @Option(name = "-result_dir", usage = "Directory to put results")
    private String resultDir = "results";

    public static GlobalConfig config() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public String getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(String seleniumHub) {
        this.seleniumHub = seleniumHub;
    }

    public static GlobalConfig getInstance() {
        return instance;
    }

    public static void setInstance(GlobalConfig instance) {
        GlobalConfig.instance = instance;
    }

    public String getResultDir() {
        return resultDir;
    }

    public void setResultDir(String resultDir) {
        this.resultDir = resultDir;
    }

    @Override
    public String toString() {
        return "GlobalConfig{" +
                "browserType=" + browserType +
                ", suites=" + suites +
                ", parallelMode=" + parallelMode +
                ", threadCount=" + threadCount +
                ", seleniumHub='" + seleniumHub + '\'' +
                ", resultDir='" + resultDir + '\'' +
                '}';
    }
}
